<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


/** Registration ROUTES*/
Route::group(['prefix' => 'registration'], function () {

    Route::get('/create', 'RegistrationController@create')->name('registration.create');
    Route::put('/store', 'RegistrationController@store')->name('registration.store');

});

/**Login ROUTES*/
Route::group(['prefix' => 'login'], function () {

    Route::get('/destroy', 'LoginsController@destroy')->name('login.destroy');
    Route::get('/create','LoginsController@create')->name('login.create');
    Route::post('/store','LoginsController@store')->name('login.store');
});

/**Word ROUTES*/
//Route::group(['prefix' => 'word', 'middleware' => ['role']], function () {
Route::group(['prefix' => 'word'], function () {
    Route::get('/', 'WordsController@index')->name('word.index');
});
Route::group(['prefix' => 'word', 'middleware' => ['role']], function () {
    Route::get('/create', 'WordsController@create')->name('word.create');
    Route::put('/store', 'WordsController@store')->name('word.store');
    Route::get('/edit/{word}', 'WordsController@edit') ->name('word.edit');
    Route::put('/update/{word}', 'WordsController@update')->name('word.update');
    Route::get('/show/{word}', 'WordsController@show')->name('word.show');
    Route::get('/delete/{word}', 'WordsController@delete')->name('word.delete');
    Route::delete('/destroy/{word}', 'WordsController@destroy')->name('word.destroy');
});