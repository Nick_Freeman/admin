<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/">Главная</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">



            @if(Auth::check())
                @if(Auth::user()->role == 'admin')

                <li class="nav-item">
                    <a class="nav-link" href="{{route('word.create')}}">Добавить слово-></a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="{{route('word.index')}}">Посмотреть слова-></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link disabled">Привет, {{Auth::user()->name}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('login.destroy')}}">Выйти</a>
                </li>
            @else

                <li class="nav-item">
                    <a class="nav-link" href="{{route('registration.create')}}">Регистрация-></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login.create')}}">Войти-></a>
                </li>
            @endif
        </ul>
    </div>
</nav>