@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">Регистрация:</h1>
@endsection

@section('main_content')
    <form action="{{route('registration.store')}}" method="post">

        @include('layouts.embed.errors')

     {{method_field('put')}}
        {{csrf_field()}}

        <div class="form-group">
            <label for="name">Имя:</label>
            <input required type="text" id="name" name="name" class="form-control">
        </div>

        <div class="form-group">
            <label for="email">Электронная почта:</label>
            <input required type="email" id="email" name="email" class="form-control">
        </div>

        <div class="form-group">
            <label for="password">Пароль:</label>
            <input required type="password" id="password" name="password" class="form-control">
        </div>

        <div class="form-group">
            <label for="password_confirmation">Повторите пароль:</label>
            <input required type="password" id="password_confirmation" name="password_confirmation" class="form-control">
        </div>

        <input name="role" type="hidden" value="{{$role='user'}}" >

        <div class="form-group">
            <button class="btn btn-success">Регистрация</button>
        </div>
    </form>
@endsection