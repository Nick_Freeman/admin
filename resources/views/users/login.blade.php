@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">Войти:</h1>
@endsection

@section('main_content')
    <form action="{{route('login.store')}}" method="post">

        @include('layouts.embed.errors')
        {{csrf_field()}}

        <div class="form-group">
            <label for="email">Электронная почта:</label>
            <input required type="email" id="email" name="email" class="form-control">
        </div>

        <div class="form-group">
            <label for="password">Пароль:</label>
            <input required type="password" id="password" name="password" class="form-control">
        </div>


        <div class="form-group">
            <button class="btn btn-success">Войти</button>
        </div>
    </form>
@endsection