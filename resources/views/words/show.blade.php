@extends('layouts/main')

@section('main_content')



@endsection

@section('jumbotron')
    <h2>Приветствуем вас на сайте .</h2>

    <p>{{ $word->eng_word }}</p>
    <p>{{ $word->rus_word }}</p>
    <p>{{ $word->eng_phrase }}</p>
    <p>{{ $word->rus_phrase }}</p>
    <img src="{{'/uploads/'.$word->image}}" class="img-fluid" alt="Responsive image" >
    <h3>{{ $word->pic_copyright }}</h3>
    <p>Short audio</p>
    <audio controls type="audio/mpeg" src="{{'/uploads/'.$word->short_audio}}">{{ $word->short_audio }}</audio>
    <p>Long audio</p>
    <audio controls type="audio/mpeg"  src="{{'/uploads/'.$word->long_audio}}">{{ $word->long_audio }}</audio>


@endsection