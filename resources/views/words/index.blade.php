@extends('layouts/main')

@section('main_content')



@endsection

@section('jumbotron')
    <form>
        <input class="place_for_search" type="text" id="text-to-find" value="" placeholder="Search" autofocus>
        <input class="button_for_turn_back" type="button" onclick="javascript: FindOnPage('text-to-find',false); return false;" value="X" title="Отменить поиск">
        <input class="button_for_search" type="submit" onclick="javascript: FindOnPage('text-to-find',true); return false;" value="find " title="Начать поиск">
    </form>
    <div class="word_table">
        <table class="table-bordered table-dark" >
            <tr>
                <th>English word</th>
                <th>Russian word</th>
                <th>English phrase</th>
                <th>Russian phrase</th>
                <th>Image</th>
                <th>Picture copyright</th>
                <th>Short audio</th>
                <th>Long audio</th>
                <th> - </th>
                <th> - </th>
                <th> - </th>
            </tr>
            @foreach($words as $word)
                {{csrf_field()}}
            <tr id="result">
             <td id="list">{{ $word->eng_word }}</td>
                <td>{{ $word->rus_word }}</td>
                <td>{{ $word->eng_phrase }}</td>
                <td>{{ $word->rus_phrase }}</td>
                <td>{{ $word->image }}</td>
                <td>{{ $word->pic_copyright }}</td>
                <td>{{ $word->short_audio }}</td>
                <td>{{ $word->long_audio }}</td>
                @if(Auth::check())
                    @if(Auth::user()->role == 'admin')
                <td><a class="btn btn-warning" href="{{route('word.show', ["word" => $word->id])}}" role="button">Show &raquo;</a></td>
                <td><a class="btn btn-warning" href="{{route('word.edit', ["word" => $word->id])}}" role="button">Edit &raquo;</a></td>
                <td><a class="btn btn-danger" href="{{route('word.delete', ["word" => $word->id])}}" role="button">Delete &raquo;</a></td>
                    @endif
                @endif
           </tr>
            @endforeach

        </table>

    </div>

@endsection