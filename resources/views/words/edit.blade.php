@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">Edit Word:</h1>
@endsection

@section('main_content')
    <form action="{{route('word.update', ["word" => $word->id])}}" method="post" enctype="multipart/form-data">

        @include('layouts.embed.errors')

        {{method_field('put')}}
        {{csrf_field()}}

        <div class="form-group">
            <label for="eng_word">Enter English Word:</label>
            <input required type="text" id="eng_word" name="eng_word" class="form-control">
        </div>

        <div class="form-group">
            <label for="rus_word">Enter Russian Word:</label>
            <input required type="text" id="rus_word" name="rus_word" class="form-control">
        </div>

        <div class="form-group">
            <label for="eng_phrase">Enter English Phrase:</label>
            <input required type="text" id="eng_phrase" name="eng_phrase" class="form-control">
        </div>

        <div class="form-group">
            <label for="rus_phrase">Enter Russian Phrase:</label>
            <input required type="text" id="rus_phrase" name="rus_phrase" class="form-control">
        </div>

        <div class="form-group">
            <label for="image">Upload image:</label>
            <input class="form-control"  type="file" name="image" id="image" class="img-thumbnail">
            <span  id="list" ></span>
        </div>

        <div class="form-group">
            <label for="pic_copyright">Enter Picture Copyright:</label>
            <input required type="text" id="pic_copyright" name="pic_copyright" class="form-control">
        </div>

        <div class="form-group">
            <label for="short_audio">Upload short audio:</label>
            <input class="form-control"  type="file" name="short_audio" id="short_audio">
        </div>

        <div class="form-group">
            <label for="long_audio">Upload long audio:</label>
            <input class="form-control"  type="file" name="long_audio" id="long_audio">
        </div>

        <div class="form-group">
            <button class="btn btn-warning"> Edit -> </button>
        </div>
    </form>
@endsection
