@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">{{$word->eng_word}}</h1>
    <p>{{$word->rus_word}}</p>
@endsection

@section('main_content')
    <div class="col-md-12">
        <form action="{{route('word.destroy', ["word" => $word->id])}}" method="post">
            {{method_field('delete')}}
            {{csrf_field()}}
            <h2>Are you sure you want to delete <b>{{$word->eng_word}}</b> ?</h2>
            <div class="form-group">
                <button class="btn btn-danger">yes</button>
                <a class="btn btn-warning" href="{{route('word.index')}}">no</a>
            </div>
        </form>
    </div>
@endsection