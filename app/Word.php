<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Word extends Model
{
    use Notifiable;

    protected $table = 'words';


    protected $fillable = ['eng_word','rus_word','eng_phrase','rus_phrase','pic_copyright','image','short_audio','long_audio'];

}
