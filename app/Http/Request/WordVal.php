<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class WordVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::route()->getName()){
            case 'word.store':
                return [
                    'eng_word' => 'required|max:8|min:3',
                    'rus_word' => 'required|max:8|min:3',
                    'eng_phrase' => 'required|max:20|min:6',
                    'rus_phrase' => 'required|max:20|min:6',
                    'pic_copyright' =>'required',
                    'image' => 'required',
                    'short_audio' =>'required',
                    'long_audio' => 'required'
                ];
                break;
        };
    }
    //php artisan make:request StoreBlogPost
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'eng_word.max' => 'Field is required,and max length 8 symbol',
            'eng_word.min' => 'Field is required,and min length 3 symbol',
            'rus_word.max' => 'Field is required,and min length 8 symbol',
            'rus_word.min' => 'Field is required,and min length 3 symbol',
            'eng_phrase.max' => 'Field is required,and max length 20 symbol',
            'eng_phrase.min' => 'Field is required,and min length 6 symbol',
            'rus_phrase.max' => 'Field is required,and max length 20 symbol',
            'rus_phrase.min' => 'Field is required,and min length 6 symbol',
            'pic_copyright' => 'Field is required',
            'image' => 'Field is required',
            'short_audio' => 'Field is required',
            'long_audio' => 'Field is required'
        ];
    }
}