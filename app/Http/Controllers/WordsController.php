<?php

namespace App\Http\Controllers;


use App\Word;
use App\Http\Request\WordVal;
use Illuminate\Http\Request;

class WordsController extends BaseController
{
    public function __construct()
    {
        $this->model = 'App\Word';
        $this->rootView = 'words';
        $this->outPutVariable = 'words';
        $this->outPutVariableForSingle = 'word';
        $this->baseRouteName = 'word';
    }

    protected function template ($word ,$request){
//audio
        $long_audio = request()->file('long_audio');
        $long_audio->move(public_path().'/uploads', $long_audio->getClientOriginalName());
        $short_audio = request()->file('short_audio');
        $short_audio->move(public_path().'/uploads', $short_audio->getClientOriginalName());
//image
        $image = request()->file('image');
        $image->move(public_path().'/uploads', $image->getClientOriginalName());
//all fields
        $word->fill($request->only('eng_word','rus_word','eng_phrase','rus_phrase','pic_copyright','image','short_audio','long_audio'));
        $word->image = $image->getClientOriginalName();
        $word->long_audio = $long_audio->getClientOriginalName();
        $word->short_audio = $short_audio->getClientOriginalName();
        $word->save();

        return json_encode([
            'eng_word'=>$word->eng_word,
            'rus_word' => $word->rus_word,
            'eng_phrase'=>$word->eng_phrase,
            'rus_phrase'=>$word->rus_phrase,
            'pic_copyright'=>$word->pic_copyright,
            'image'=>$word->image,
            'short_audio'=>$word->short_audio,
            'long_audio'=>$word->long_audio
        ]);
    }


    public function tableToAjax()
    {
        return response(Word::all());
    }

    public function create(){
        return view('words.create');
    }

        public function update (Request $request){
            $word = Word::find($request->word);
            if($word){
               $this->template($word,$request);
             return redirect()->route('word.index') ;
            } else {
                return redirect()->back();
            }
        }

    public function store (WordVal $request){
        $word = new Word();
         $this->template($word,$request);


            return redirect(route('word.index'));

    }
}
