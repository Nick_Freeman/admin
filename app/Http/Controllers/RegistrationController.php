<?php

namespace App\Http\Controllers;

use App\Http\Request\RegVal;
use App\User;


class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\User';
        $this->rootView = 'users';
        $this->outPutVariable = 'users';
        $this->outPutVariableForSingle = 'user';
        $this->baseRouteName = 'user';
    }


    public function create(){
        return view('users.create');
    }

    public function store(RegVal $request){


        $user = new User();

        $user->fill($request->only('name','role','email','password'));

        $user ->password = bcrypt($user["password"]);
        $user->save();

        auth()->login($user);


        return redirect('/');
    }
}