<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginsController extends Controller
{
    public function destroy(){
        auth()->logout();
        return redirect('/');
    }

    public function create(){
        return view('users.login');
    }

    public function store(Request $request){

        $email = $request->input('email');

        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])){

               return redirect('/');
            }

        return ('smthng wrong');
    }
}
